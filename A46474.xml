<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A46474">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>His Majesties gracious letter to the lord provost, bailzies, and remanent magistrates, and town council of the city of Edinburgh</title>
    <author>Scotland. Sovereign (1685-1688 : James VII)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A46474 of text R41408 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing J194A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A46474</idno>
    <idno type="STC">Wing J194A</idno>
    <idno type="STC">ESTC R41408</idno>
    <idno type="EEBO-CITATION">31355295</idno>
    <idno type="OCLC">ocm 31355295</idno>
    <idno type="VID">110385</idno>
    <idno type="PROQUESTGOID">2240895808</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A46474)</note>
    <note>Transcribed from: (Early English Books Online ; image set 110385)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1741:1)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>His Majesties gracious letter to the lord provost, bailzies, and remanent magistrates, and town council of the city of Edinburgh</title>
      <author>Scotland. Sovereign (1685-1688 : James VII)</author>
      <author>James II, King of England, 1633-1701.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.).</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[Edinburgh? :</pubPlace>
      <date>1685]</date>
     </publicationStmt>
     <notesStmt>
      <note>Place and date of publication from Wing (2nd ed.).</note>
      <note>Imperfect: cut at center fold, with loss of text.</note>
      <note>"Given at our Court at Whitehall the 28. day of February 1684/5 and of our Reign the 1st year. By His Majesties command. Drummond."</note>
      <note>Reproduction of original in the Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Edinburgh (Scotland) -- History -- 17th century.</term>
     <term>Scotland -- History -- 1660-1688.</term>
     <term>Great Britain -- History -- James II, 1685-1688.</term>
     <term>Broadsides -- Edinburgh (Scotland) -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>His Majesties gracious letter, to the lord provost, bailzies, and remanent magistrates, and town council of the city of Edinburgh.</ep:title>
    <ep:author>Scotland. Sovereign </ep:author>
    <ep:publicationYear>1685</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>304</ep:wordCount>
    <ep:defectiveTokenCount>2</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>65.79</ep:defectRate>
    <ep:finalGrade>D</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 65.79 defects per 10,000 words puts this text in the D category of texts with between 35 and 100 defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-11</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-01</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-02</date>
    <label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Rekeyed and resubmitted
      </change>
   <change>
    <date>2008-11</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-11</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A46474-t">
  <body xml:id="A46474-e0">
   <div type="letter" xml:id="A46474-e10">
    <pb facs="tcp:110385:1" rend="simple:additions" xml:id="A46474-001-a"/>
    <head xml:id="A46474-e20">
     <figure xml:id="A46474-e30">
      <p xml:id="A46474-e40">
       <w lemma="i" pos="pns" reg="I" xml:id="A46474-001-a-0010">J</w>
       <w lemma="r" pos="sy" xml:id="A46474-001-a-0020">R</w>
      </p>
      <p xml:id="A46474-e50">
       <w lemma="n/a" pos="ffr" xml:id="A46474-001-a-0030">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A46474-001-a-0040">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A46474-001-a-0050">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A46474-001-a-0060">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A46474-001-a-0070">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A46474-001-a-0080">PENSE</w>
      </p>
      <p xml:id="A46474-e60">
       <w lemma="GOD" pos="nn1" xml:id="A46474-001-a-0090">GOD</w>
       <w lemma="save" pos="acp" xml:id="A46474-001-a-0100">SAVE</w>
       <w lemma="the" pos="d" xml:id="A46474-001-a-0110">THE</w>
       <w lemma="king" pos="n1" xml:id="A46474-001-a-0120">KING</w>
      </p>
      <figDesc xml:id="A46474-e70">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="A46474-e80">
     <hi xml:id="A46474-e90">
      <w lemma="his" pos="po" xml:id="A46474-001-a-0130">His</w>
      <w lemma="Majesty" pos="ng1" xml:id="A46474-001-a-0140">MAJESTIE'S</w>
      <w lemma="gracious" pos="j" xml:id="A46474-001-a-0150">Gracious</w>
     </hi>
     <w lemma="letter" pos="n1" xml:id="A46474-001-a-0160">LETTER</w>
     <pc xml:id="A46474-001-a-0170">,</pc>
     <w lemma="to" pos="acp" xml:id="A46474-001-a-0180">TO</w>
     <w lemma="the" pos="d" xml:id="A46474-001-a-0190">THE</w>
     <w lemma="lord" pos="n1" xml:id="A46474-001-a-0200">LORD</w>
     <w lemma="provost" pos="n1" xml:id="A46474-001-a-0210">PROVOST</w>
     <pc xml:id="A46474-001-a-0220">,</pc>
     <w lemma="bailȝy" pos="n2" xml:id="A46474-001-a-0230">BAILȜIES</w>
     <pc xml:id="A46474-001-a-0240">,</pc>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-0250">and</w>
     <w lemma="n/a" pos="fla" xml:id="A46474-001-a-0260">REMANENT</w>
     <w lemma="magistrate" pos="n2" reg="MAGISTRATES" xml:id="A46474-001-a-0270">MAGISTRATS</w>
     <pc xml:id="A46474-001-a-0280">,</pc>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-0290">and</w>
     <w lemma="town" pos="n1" xml:id="A46474-001-a-0300">TOWN</w>
     <w lemma="council" pos="n1" xml:id="A46474-001-a-0310">COUNCIL</w>
     <w lemma="of" pos="acp" xml:id="A46474-001-a-0320">of</w>
     <w lemma="the" pos="d" xml:id="A46474-001-a-0330">the</w>
     <w lemma="city" pos="n1" xml:id="A46474-001-a-0340">CITY</w>
     <w lemma="of" pos="acp" xml:id="A46474-001-a-0350">of</w>
     <w lemma="Edinburgh" pos="nn1" reg="EDINBURGH" rend="hi" xml:id="A46474-001-a-0360">EDINBVRGH</w>
     <pc unit="sentence" xml:id="A46474-001-a-0370">.</pc>
    </head>
    <head xml:id="A46474-e110">
     <add xml:id="A46474-e120">
      <date xml:id="A46474-e130">
       <w lemma="Feb" pos="ab" xml:id="A46474-001-a-0380">Feb</w>
       <w lemma="●" pos="zz" xml:id="A46474-001-a-0390">●</w>
      </date>
      <w lemma="on" pos="acp" xml:id="A46474-001-a-0400">on</w>
      <w lemma="k" pos="sy" xml:id="A46474-001-a-0410">K</w>
      <w lemma="ch" pos="ab" xml:id="A46474-001-a-0420">Ch</w>
      <w lemma="death" pos="n1" xml:id="A46474-001-a-0430">death</w>
     </add>
    </head>
    <head type="sub" xml:id="A46474-e140">
     <add xml:id="A46474-e150">
      <w lemma="the" pos="d" xml:id="A46474-001-a-0440">the</w>
      <w lemma="king" pos="n2" xml:id="A46474-001-a-0450">Kings</w>
      <w lemma="n/a" pos="ffr" rend="abbr" xml:id="A46474-001-a-0460">Ans</w>
      <unclear xml:id="A46474-e170">
       <w lemma="to" pos="acp" xml:id="A46474-001-a-0470">to</w>
      </unclear>
      <w lemma="the" pos="d" xml:id="A46474-001-a-0480">the</w>
      <w lemma="town" pos="n1" xml:id="A46474-001-a-0490">town</w>
      <w lemma="of" pos="acp" xml:id="A46474-001-a-0500">of</w>
      <w lemma="edinburgh" pos="nng1" reg="edinburgh's" xml:id="A46474-001-a-0510">Edinburghs</w>
      <w lemma="address" pos="vvb" reg="Address" xml:id="A46474-001-a-0520">Addresse</w>
     </add>
    </head>
    <opener xml:id="A46474-e180">
     <signed xml:id="A46474-e190">
      <w lemma="JAMES" pos="nn1" xml:id="A46474-001-a-0530">JAMES</w>
      <w lemma="r." pos="ab" xml:id="A46474-001-a-0540">R.</w>
      <pc unit="sentence" xml:id="A46474-001-a-0550"/>
     </signed>
    </opener>
    <p xml:id="A46474-e200">
     <w lemma="trusty" pos="j" rend="decorinit" xml:id="A46474-001-a-0560">TRUSTY</w>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-0570">and</w>
     <w lemma="well-beloved" pos="j" reg="well-beloved" xml:id="A46474-001-a-0580">welbeloved</w>
     <pc xml:id="A46474-001-a-0590">,</pc>
     <w lemma="we" pos="pns" reg="We" xml:id="A46474-001-a-0600">Wee</w>
     <w lemma="greet" pos="vvb" xml:id="A46474-001-a-0610">greet</w>
     <w lemma="you" pos="pn" xml:id="A46474-001-a-0620">you</w>
     <w lemma="well" pos="av" xml:id="A46474-001-a-0630">well</w>
     <pc unit="sentence" xml:id="A46474-001-a-0640">.</pc>
     <w lemma="have" pos="vvg" xml:id="A46474-001-a-0650">Having</w>
     <w lemma="receive" pos="vvn" xml:id="A46474-001-a-0660">received</w>
     <w lemma="yesterday" pos="av" xml:id="A46474-001-a-0670">yesterday</w>
     <w lemma="from" pos="acp" xml:id="A46474-001-a-0680">from</w>
     <w lemma="our" pos="po" xml:id="A46474-001-a-0690">our</w>
     <w lemma="secretary" pos="n1" xml:id="A46474-001-a-0700">Secretary</w>
     <w lemma="LUNDIN" pos="nn1" xml:id="A46474-001-a-0710">LUNDIN</w>
     <w lemma="your" pos="po" xml:id="A46474-001-a-0720">your</w>
     <w lemma="very" pos="j" xml:id="A46474-001-a-0730">very</w>
     <w lemma="loyal" pos="j" xml:id="A46474-001-a-0740">Loyal</w>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-0750">and</w>
     <w lemma="dutiful" pos="j" xml:id="A46474-001-a-0760">Dutiful</w>
     <w lemma="address" pos="n1" xml:id="A46474-001-a-0770">Address</w>
     <pc xml:id="A46474-001-a-0780">,</pc>
     <w lemma="we" pos="pns" reg="We" xml:id="A46474-001-a-0790">Wee</w>
     <w lemma="have" pos="vvb" xml:id="A46474-001-a-0800">have</w>
     <w lemma="think" pos="vvn" xml:id="A46474-001-a-0810">thought</w>
     <w lemma="fit" pos="j" xml:id="A46474-001-a-0820">fit</w>
     <w lemma="to" pos="prt" xml:id="A46474-001-a-0830">to</w>
     <w lemma="let" pos="vvi" xml:id="A46474-001-a-0840">let</w>
     <w lemma="you" pos="pn" xml:id="A46474-001-a-0850">you</w>
     <w lemma="know" pos="vvb" xml:id="A46474-001-a-0860">know</w>
     <pc xml:id="A46474-001-a-0870">,</pc>
     <w lemma="that" pos="cs" xml:id="A46474-001-a-0880">that</w>
     <w lemma="it" pos="pn" xml:id="A46474-001-a-0890">it</w>
     <w lemma="be" pos="vvd" xml:id="A46474-001-a-0900">was</w>
     <w lemma="very" pos="av" xml:id="A46474-001-a-0910">very</w>
     <w lemma="acceptable" pos="j" xml:id="A46474-001-a-0920">acceptable</w>
     <w lemma="to" pos="acp" xml:id="A46474-001-a-0930">to</w>
     <w lemma="we" pos="pno" xml:id="A46474-001-a-0940">US</w>
     <pc xml:id="A46474-001-a-0950">,</pc>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-0960">and</w>
     <w lemma="suitable" pos="j" xml:id="A46474-001-a-0970">suitable</w>
     <w lemma="to" pos="prt" xml:id="A46474-001-a-0980">to</w>
     <w lemma="that" pos="d" xml:id="A46474-001-a-0990">that</w>
     <w lemma="zeal" pos="n1" xml:id="A46474-001-a-1000">Zeal</w>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-1010">and</w>
     <w lemma="loyalty" pos="n1" xml:id="A46474-001-a-1020">Loyalty</w>
     <w lemma="you" pos="pn" xml:id="A46474-001-a-1030">you</w>
     <w lemma="have" pos="vvb" xml:id="A46474-001-a-1040">have</w>
     <w lemma="at" pos="acp" xml:id="A46474-001-a-1050">at</w>
     <w lemma="all" pos="d" xml:id="A46474-001-a-1060">all</w>
     <w lemma="time" pos="n2" xml:id="A46474-001-a-1070">times</w>
     <w lemma="express" pos="vvn" xml:id="A46474-001-a-1080">expressed</w>
     <w lemma="in" pos="acp" xml:id="A46474-001-a-1090">in</w>
     <w lemma="the" pos="d" xml:id="A46474-001-a-1100">the</w>
     <w lemma="reign" pos="n1" xml:id="A46474-001-a-1110">Reign</w>
     <w lemma="of" pos="acp" xml:id="A46474-001-a-1120">of</w>
     <w lemma="our" pos="po" xml:id="A46474-001-a-1130">our</w>
     <w lemma="late" pos="j" xml:id="A46474-001-a-1140">late</w>
     <w lemma="dear" pos="js" xml:id="A46474-001-a-1150">Dearest</w>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-1160">and</w>
     <w lemma="royal" pos="j" xml:id="A46474-001-a-1170">Royal</w>
     <w lemma="brother" pos="n1" xml:id="A46474-001-a-1180">Brother</w>
     <w lemma="of" pos="acp" xml:id="A46474-001-a-1190">of</w>
     <w lemma="bless" pos="j-vn" xml:id="A46474-001-a-1200">Blessed</w>
     <w lemma="memory" pos="n1" xml:id="A46474-001-a-1210">Memory</w>
     <pc xml:id="A46474-001-a-1220">:</pc>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-1230">And</w>
     <w lemma="from" pos="acp" xml:id="A46474-001-a-1240">from</w>
     <w lemma="these" pos="d" xml:id="A46474-001-a-1250">these</w>
     <w lemma="early" pos="j" xml:id="A46474-001-a-1260">early</w>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-1270">and</w>
     <w lemma="ample" pos="j" xml:id="A46474-001-a-1280">ample</w>
     <w lemma="expression" pos="n2" xml:id="A46474-001-a-1290">Expressions</w>
     <w lemma="of" pos="acp" xml:id="A46474-001-a-1300">of</w>
     <w lemma="your" pos="po" xml:id="A46474-001-a-1310">your</w>
     <w lemma="duty" pos="n1" xml:id="A46474-001-a-1320">Duty</w>
     <w lemma="to" pos="acp" xml:id="A46474-001-a-1330">to</w>
     <w lemma="we" pos="pno" xml:id="A46474-001-a-1340">Us</w>
     <pc xml:id="A46474-001-a-1350">,</pc>
     <w lemma="we" pos="pns" reg="We" xml:id="A46474-001-a-1360">Wee</w>
     <w lemma="be" pos="vvb" xml:id="A46474-001-a-1370">are</w>
     <w lemma="so" pos="av" xml:id="A46474-001-a-1380">so</w>
     <w lemma="much" pos="av-d" xml:id="A46474-001-a-1390">much</w>
     <w lemma="persuade" pos="vvn" reg="persuaded" xml:id="A46474-001-a-1400">perswaded</w>
     <w lemma="of" pos="acp" xml:id="A46474-001-a-1410">of</w>
     <w lemma="your" pos="po" xml:id="A46474-001-a-1420">your</w>
     <w lemma="sincere" pos="j" xml:id="A46474-001-a-1430">sincere</w>
     <w lemma="resolution" pos="n2" xml:id="A46474-001-a-1440">Resolutions</w>
     <w lemma="to" pos="prt" xml:id="A46474-001-a-1450">to</w>
     <w lemma="continue" pos="vvi" xml:id="A46474-001-a-1460">continue</w>
     <w lemma="the" pos="d" xml:id="A46474-001-a-1470">the</w>
     <w lemma="〈…〉" pos="zz" xml:id="A46474-001-a-1480">〈…〉</w>
     <unclear xml:id="A46474-e210">
      <w lemma="to" pos="prt" xml:id="A46474-001-a-1490">to</w>
      <w lemma="assure" pos="vvb" xml:id="A46474-001-a-1500">assure</w>
     </unclear>
     <w lemma="you" pos="pn" xml:id="A46474-001-a-1510">you</w>
     <pc xml:id="A46474-001-a-1520">,</pc>
     <w lemma="that" pos="cs" xml:id="A46474-001-a-1530">that</w>
     <w lemma="upon" pos="acp" xml:id="A46474-001-a-1540">upon</w>
     <w lemma="all" pos="d" xml:id="A46474-001-a-1550">all</w>
     <w lemma="occasion" pos="n2" xml:id="A46474-001-a-1560">occasions</w>
     <w lemma="we" pos="pns" reg="we" xml:id="A46474-001-a-1570">wee</w>
     <w lemma="will" pos="vmb" xml:id="A46474-001-a-1580">will</w>
     <w lemma="show" pos="vvi" xml:id="A46474-001-a-1590">show</w>
     <w lemma="our" pos="po" xml:id="A46474-001-a-1600">our</w>
     <w lemma="kindness" pos="n1" xml:id="A46474-001-a-1610">kindness</w>
     <w lemma="to" pos="acp" xml:id="A46474-001-a-1620">to</w>
     <w lemma="you" pos="pn" xml:id="A46474-001-a-1630">you</w>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-1640">and</w>
     <w lemma="that" pos="cs" xml:id="A46474-001-a-1650">that</w>
     <w lemma="our" pos="po" xml:id="A46474-001-a-1660">our</w>
     <w lemma="good" pos="j" xml:id="A46474-001-a-1670">good</w>
     <w lemma="town" pos="n1" xml:id="A46474-001-a-1680">Town</w>
     <pc xml:id="A46474-001-a-1690">;</pc>
     <w lemma="of" pos="acp" xml:id="A46474-001-a-1700">Of</w>
     <w lemma="who" pos="crq" xml:id="A46474-001-a-1710">whose</w>
     <w lemma="concern" pos="vvz" xml:id="A46474-001-a-1720">concerns</w>
     <w lemma="in" pos="acp" xml:id="A46474-001-a-1730">in</w>
     <w lemma="every" pos="d" xml:id="A46474-001-a-1740">every</w>
     <w lemma="thing" pos="n1" xml:id="A46474-001-a-1750">thing</w>
     <w lemma="that" pos="cs" xml:id="A46474-001-a-1760">that</w>
     <w lemma="may" pos="vmb" xml:id="A46474-001-a-1770">may</w>
     <w lemma="contribute" pos="vvi" xml:id="A46474-001-a-1780">contribute</w>
     <w lemma="to" pos="acp" xml:id="A46474-001-a-1790">to</w>
     <w lemma="your" pos="po" xml:id="A46474-001-a-1800">your</w>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-1810">and</w>
     <w lemma="their" pos="po" xml:id="A46474-001-a-1820">their</w>
     <w lemma="wellbeing" pos="n1" reg="wellbeing" xml:id="A46474-001-a-1830">welbeing</w>
     <w lemma="we" pos="pns" reg="We" xml:id="A46474-001-a-1840">Wee</w>
     <w lemma="will" pos="vmb" xml:id="A46474-001-a-1850">will</w>
     <w lemma="have" pos="vvi" xml:id="A46474-001-a-1860">have</w>
     <w lemma="a" pos="d" xml:id="A46474-001-a-1870">a</w>
     <w lemma="peculiar" pos="j" xml:id="A46474-001-a-1880">peculiar</w>
     <w lemma="care" pos="n1" xml:id="A46474-001-a-1890">care</w>
     <pc xml:id="A46474-001-a-1900">;</pc>
     <w lemma="assure" pos="vvg" xml:id="A46474-001-a-1910">Assuring</w>
     <w lemma="you" pos="pn" xml:id="A46474-001-a-1920">you</w>
     <w lemma="withal" pos="av" reg="withal" xml:id="A46474-001-a-1930">withall</w>
     <pc xml:id="A46474-001-a-1940">,</pc>
     <w lemma="that" pos="cs" xml:id="A46474-001-a-1950">that</w>
     <w lemma="we" pos="pns" reg="We" xml:id="A46474-001-a-1960">Wee</w>
     <w lemma="be" pos="vvb" xml:id="A46474-001-a-1970">are</w>
     <w lemma="so" pos="av" xml:id="A46474-001-a-1980">so</w>
     <w lemma="sensible" pos="j" xml:id="A46474-001-a-1990">sensible</w>
     <w lemma="of" pos="acp" xml:id="A46474-001-a-2000">of</w>
     <w lemma="your" pos="po" xml:id="A46474-001-a-2010">your</w>
     <w lemma="former" pos="j" xml:id="A46474-001-a-2020">former</w>
     <w lemma="service" pos="n2" xml:id="A46474-001-a-2030">Services</w>
     <w lemma="since" pos="acp" xml:id="A46474-001-a-2040">since</w>
     <w lemma="you" pos="pn" xml:id="A46474-001-a-2050">you</w>
     <w lemma="enter" pos="vvd" reg="entered" xml:id="A46474-001-a-2060">entred</w>
     <w lemma="into" pos="acp" xml:id="A46474-001-a-2070">into</w>
     <w lemma="the" pos="d" xml:id="A46474-001-a-2080">the</w>
     <w lemma="magistracy" pos="n1" xml:id="A46474-001-a-2090">Magistracy</w>
     <w lemma="of" pos="acp" xml:id="A46474-001-a-2100">of</w>
     <w lemma="that" pos="d" xml:id="A46474-001-a-2110">that</w>
     <w lemma="our" pos="po" xml:id="A46474-001-a-2120">our</w>
     <w lemma="good" pos="j" xml:id="A46474-001-a-2130">good</w>
     <w lemma="town" pos="n1" xml:id="A46474-001-a-2140">Town</w>
     <pc xml:id="A46474-001-a-2150">,</pc>
     <w lemma="as" pos="acp" xml:id="A46474-001-a-2160">as</w>
     <w lemma="we" pos="pns" reg="We" xml:id="A46474-001-a-2170">Wee</w>
     <w lemma="think" pos="vvb" xml:id="A46474-001-a-2180">think</w>
     <w lemma="fit" pos="j" xml:id="A46474-001-a-2190">fit</w>
     <w lemma="to" pos="prt" xml:id="A46474-001-a-2200">to</w>
     <w lemma="return" pos="vvi" xml:id="A46474-001-a-2210">return</w>
     <w lemma="you" pos="pn" xml:id="A46474-001-a-2220">you</w>
     <w lemma="our" pos="po" xml:id="A46474-001-a-2230">our</w>
     <w lemma="hearty" pos="j" xml:id="A46474-001-a-2240">hearty</w>
     <w lemma="thanks" pos="n2" xml:id="A46474-001-a-2250">thanks</w>
     <pc xml:id="A46474-001-a-2260">,</pc>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-2270">and</w>
     <w lemma="to" pos="prt" xml:id="A46474-001-a-2280">to</w>
     <w lemma="assure" pos="vvi" xml:id="A46474-001-a-2290">assure</w>
     <w lemma="you" pos="pn" xml:id="A46474-001-a-2300">you</w>
     <pc xml:id="A46474-001-a-2310">,</pc>
     <w lemma="that" pos="cs" xml:id="A46474-001-a-2320">that</w>
     <w lemma="you" pos="pn" xml:id="A46474-001-a-2330">you</w>
     <w lemma="shall" pos="vmb" xml:id="A46474-001-a-2340">shall</w>
     <w lemma="meet" pos="vvi" xml:id="A46474-001-a-2350">meet</w>
     <w lemma="with" pos="acp" xml:id="A46474-001-a-2360">with</w>
     <w lemma="the" pos="d" xml:id="A46474-001-a-2370">the</w>
     <w lemma="good" pos="j" xml:id="A46474-001-a-2380">good</w>
     <w lemma="effect" pos="n2" xml:id="A46474-001-a-2390">effects</w>
     <w lemma="thereof" pos="av" xml:id="A46474-001-a-2400">thereof</w>
     <w lemma="when" pos="crq" xml:id="A46474-001-a-2410">when</w>
     <w lemma="a" pos="d" xml:id="A46474-001-a-2420">an</w>
     <w lemma="opportunity" pos="n1" xml:id="A46474-001-a-2430">opportunity</w>
     <w lemma="shall" pos="vmb" xml:id="A46474-001-a-2440">shall</w>
     <w lemma="be" pos="vvi" xml:id="A46474-001-a-2450">be</w>
     <w lemma="offer" pos="vvn" xml:id="A46474-001-a-2460">offered</w>
     <w lemma="to" pos="acp" xml:id="A46474-001-a-2470">to</w>
     <w lemma="we" pos="pno" xml:id="A46474-001-a-2480">Us</w>
     <w lemma="for" pos="acp" xml:id="A46474-001-a-2490">for</w>
     <w lemma="the" pos="d" xml:id="A46474-001-a-2500">the</w>
     <w lemma="same" pos="d" xml:id="A46474-001-a-2510">same</w>
     <pc unit="sentence" xml:id="A46474-001-a-2520">.</pc>
     <w lemma="so" pos="av" xml:id="A46474-001-a-2530">So</w>
     <w lemma="not" pos="xx" xml:id="A46474-001-a-2540">not</w>
     <w lemma="doubt" pos="vvg" xml:id="A46474-001-a-2550">doubting</w>
     <w lemma="your" pos="po" xml:id="A46474-001-a-2560">your</w>
     <w lemma="continue" pos="n1-vg" xml:id="A46474-001-a-2570">continuing</w>
     <w lemma="to" pos="prt" xml:id="A46474-001-a-2580">to</w>
     <w lemma="act" pos="vvi" xml:id="A46474-001-a-2590">act</w>
     <w lemma="faithful" pos="av-j" xml:id="A46474-001-a-2600">faithfully</w>
     <w lemma="and" pos="cc" xml:id="A46474-001-a-2610">and</w>
     <w lemma="vigorous" pos="av-j" xml:id="A46474-001-a-2620">vigorously</w>
     <w lemma="in" pos="acp" xml:id="A46474-001-a-2630">in</w>
     <w lemma="our" pos="po" xml:id="A46474-001-a-2640">our</w>
     <w lemma="service" pos="n1" xml:id="A46474-001-a-2650">Service</w>
     <pc xml:id="A46474-001-a-2660">,</pc>
     <w lemma="we" pos="pns" reg="We" xml:id="A46474-001-a-2670">Wee</w>
     <w lemma="bid" pos="vvb" xml:id="A46474-001-a-2680">bid</w>
     <w lemma="you" pos="pn" xml:id="A46474-001-a-2690">you</w>
     <w lemma="farewell" pos="uh" xml:id="A46474-001-a-2700">Farewell</w>
     <pc unit="sentence" xml:id="A46474-001-a-2710">.</pc>
    </p>
    <closer xml:id="A46474-e220">
     <dateline xml:id="A46474-e230">
      <w lemma="give" pos="vvn" xml:id="A46474-001-a-2720">Given</w>
      <w lemma="at" pos="acp" xml:id="A46474-001-a-2730">at</w>
      <w lemma="our" pos="po" xml:id="A46474-001-a-2740">our</w>
      <w lemma="court" pos="n1" xml:id="A46474-001-a-2750">Court</w>
      <w lemma="at" pos="acp" xml:id="A46474-001-a-2760">at</w>
      <w lemma="Whitehall" pos="nn1" rend="hi" xml:id="A46474-001-a-2770">Whitehall</w>
      <date xml:id="A46474-e250">
       <w lemma="the" pos="d" xml:id="A46474-001-a-2780">the</w>
       <w lemma="28" pos="crd" xml:id="A46474-001-a-2790">28</w>
       <w lemma="day" pos="n1" xml:id="A46474-001-a-2800">day</w>
       <w lemma="of" pos="acp" xml:id="A46474-001-a-2810">of</w>
       <hi xml:id="A46474-e260">
        <w lemma="February" pos="nn1" xml:id="A46474-001-a-2820">February</w>
        <w lemma="1684/5" pos="crd" xml:id="A46474-001-a-2840">1684/5</w>
       </hi>
       <pc rend="follows-hi" xml:id="A46474-001-a-2860">;</pc>
       <w lemma="and" pos="cc" xml:id="A46474-001-a-2870">and</w>
       <w lemma="of" pos="acp" xml:id="A46474-001-a-2880">of</w>
       <w lemma="our" pos="po" xml:id="A46474-001-a-2890">our</w>
       <w lemma="reign" pos="n1" xml:id="A46474-001-a-2900">Reign</w>
       <w lemma="the" pos="d" xml:id="A46474-001-a-2910">the</w>
       <hi xml:id="A46474-e270">
        <w lemma="1" pos="ord" rend="hi" xml:id="A46474-001-a-2930">1st</w>
        <pc xml:id="A46474-001-a-2940">.</pc>
       </hi>
       <w lemma="year" pos="n1" xml:id="A46474-001-a-2950">year</w>
       <pc unit="sentence" xml:id="A46474-001-a-2960">.</pc>
      </date>
     </dateline>
     <signed xml:id="A46474-e290">
      <w lemma="by" pos="acp" xml:id="A46474-001-a-2970">By</w>
      <w lemma="his" pos="po" xml:id="A46474-001-a-2980">His</w>
      <w lemma="majesty" pos="ng1" reg="MAJESTY'S" xml:id="A46474-001-a-2990">MAJESTIES</w>
      <w lemma="command" pos="n1" xml:id="A46474-001-a-3000">command</w>
      <pc xml:id="A46474-001-a-3010">,</pc>
      <w lemma="drummond" pos="nn1" reg="DRUMMOND" rend="hi" xml:id="A46474-001-a-3020">DRVMMOND</w>
      <pc unit="sentence" xml:id="A46474-001-a-3030">.</pc>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="A46474-t-b"/>
 </text>
</TEI>

<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A46511">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A proclamation, whereas the Parliament hath been prorogued until the tenth day of February next James R.</title>
    <author>England and Wales. Sovereign (1685-1688 : James II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A46511 of text R3308 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing J248). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A46511</idno>
    <idno type="STC">Wing J248</idno>
    <idno type="STC">ESTC R3308</idno>
    <idno type="EEBO-CITATION">12267991</idno>
    <idno type="OCLC">ocm 12267991</idno>
    <idno type="VID">58126</idno>
    <idno type="PROQUESTGOID">2240969694</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A46511)</note>
    <note>Transcribed from: (Early English Books Online ; image set 58126)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 869:16)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A proclamation, whereas the Parliament hath been prorogued until the tenth day of February next James R.</title>
      <author>England and Wales. Sovereign (1685-1688 : James II)</author>
      <author>James II, King of England, 1633-1701.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by the assigns of John Bill deceas'd, and by Henry Hills, and Thomas Newcomb ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1685/6.</date>
     </publicationStmt>
     <notesStmt>
      <note>Reproduction of original in Huntington Library.</note>
      <note>Broadside.</note>
      <note>At head of title: By the King, a proclamation.</note>
      <note>At end of text: Given at our court at Whitehall the eighth day of January 1685/6.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament -- Rules and practice.</term>
     <term>Broadsides</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King, a proclamation. James R. Whereas the Parliament hath been prorogued until the tenth day of February next, ...</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1686</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>323</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-11</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-01</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-02</date>
    <label>Pip Willcox</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Rekeyed and resubmitted
      </change>
   <change>
    <date>2008-11</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-11</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A46511-t">
  <body xml:id="A46511-e0">
   <div type="royal_proclamation" xml:id="A46511-e10">
    <pb facs="tcp:58126:1" xml:id="A46511-001-a"/>
    <head xml:id="A46511-e20">
     <figure xml:id="A46511-e30">
      <p xml:id="A46511-e40">
       <w lemma="J2R" orig="J²R" pos="sy" xml:id="A46511-001-a-0010">J2R</w>
      </p>
      <p xml:id="A46511-e60">
       <w lemma="n/a" pos="ffr" xml:id="A46511-001-a-0040">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="A46511-001-a-0050">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="A46511-001-a-0060">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="A46511-001-a-0070">DROIT</w>
      </p>
      <p xml:id="A46511-e70">
       <w lemma="n/a" pos="ffr" xml:id="A46511-001-a-0080">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="A46511-001-a-0090">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="A46511-001-a-0100">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="A46511-001-a-0110">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="A46511-001-a-0120">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="A46511-001-a-0130">PENSE</w>
      </p>
      <figDesc xml:id="A46511-e80">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <byline xml:id="A46511-e90">
     <w lemma="by" pos="acp" xml:id="A46511-001-a-0140">By</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-0150">the</w>
     <w lemma="king" pos="n1" xml:id="A46511-001-a-0160">King</w>
     <pc xml:id="A46511-001-a-0170">,</pc>
    </byline>
    <head xml:id="A46511-e100">
     <w lemma="a" pos="d" xml:id="A46511-001-a-0180">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A46511-001-a-0190">PROCLAMATION</w>
     <pc unit="sentence" xml:id="A46511-001-a-0200">.</pc>
    </head>
    <opener xml:id="A46511-e110">
     <signed xml:id="A46511-e120">
      <w lemma="JAMES" pos="nn1" xml:id="A46511-001-a-0210">JAMES</w>
      <w lemma="r." pos="ab" xml:id="A46511-001-a-0220">R.</w>
      <pc unit="sentence" xml:id="A46511-001-a-0230"/>
     </signed>
    </opener>
    <p xml:id="A46511-e130">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A46511-001-a-0240">WHereas</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-0250">the</w>
     <w lemma="parliament" pos="n1" xml:id="A46511-001-a-0260">Parliament</w>
     <w lemma="have" pos="vvz" xml:id="A46511-001-a-0270">hath</w>
     <w lemma="be" pos="vvn" xml:id="A46511-001-a-0280">been</w>
     <w lemma="prorogue" pos="vvn" xml:id="A46511-001-a-0290">Prorogued</w>
     <w lemma="until" pos="acp" xml:id="A46511-001-a-0300">until</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-0310">the</w>
     <w lemma="ten" pos="ord" xml:id="A46511-001-a-0320">Tenth</w>
     <w lemma="day" pos="n1" xml:id="A46511-001-a-0330">day</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-0340">of</w>
     <w lemma="February" pos="nn1" rend="hi" xml:id="A46511-001-a-0350">February</w>
     <w lemma="next" pos="ord" xml:id="A46511-001-a-0360">next</w>
     <pc xml:id="A46511-001-a-0370">,</pc>
     <w lemma="we" pos="pns" xml:id="A46511-001-a-0380">We</w>
     <w lemma="for" pos="acp" xml:id="A46511-001-a-0390">for</w>
     <w lemma="many" pos="d" xml:id="A46511-001-a-0400">many</w>
     <w lemma="weighty" pos="j" xml:id="A46511-001-a-0410">weighty</w>
     <w lemma="reason" pos="n2" xml:id="A46511-001-a-0420">Reasons</w>
     <pc xml:id="A46511-001-a-0430">,</pc>
     <w lemma="have" pos="vvb" xml:id="A46511-001-a-0440">have</w>
     <w lemma="think" pos="vvn" xml:id="A46511-001-a-0450">thought</w>
     <w lemma="fit" pos="j" xml:id="A46511-001-a-0460">fit</w>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-0470">and</w>
     <w lemma="resolve" pos="vvn" xml:id="A46511-001-a-0480">Resolved</w>
     <w lemma="to" pos="prt" xml:id="A46511-001-a-0490">to</w>
     <w lemma="make" pos="vvi" xml:id="A46511-001-a-0500">make</w>
     <w lemma="a" pos="d" xml:id="A46511-001-a-0510">a</w>
     <w lemma="further" pos="jc" xml:id="A46511-001-a-0520">further</w>
     <w lemma="prorogation" pos="n1" xml:id="A46511-001-a-0530">Prorogation</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-0540">of</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-0550">the</w>
     <w lemma="parliament" pos="n1" xml:id="A46511-001-a-0560">Parliament</w>
     <pc xml:id="A46511-001-a-0570">,</pc>
     <w lemma="until" pos="acp" xml:id="A46511-001-a-0580">until</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-0590">the</w>
     <w lemma="ten" pos="ord" xml:id="A46511-001-a-0600">Tenth</w>
     <w lemma="day" pos="n1" xml:id="A46511-001-a-0610">day</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-0620">of</w>
     <w lemma="May" pos="nn1" rend="hi" xml:id="A46511-001-a-0630">May</w>
     <w lemma="next" pos="ord" xml:id="A46511-001-a-0640">next</w>
     <w lemma="ensue" pos="vvg" xml:id="A46511-001-a-0650">ensuing</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-0660">the</w>
     <w lemma="date" pos="n1" xml:id="A46511-001-a-0670">Date</w>
     <w lemma="hereof" pos="av" xml:id="A46511-001-a-0680">hereof</w>
     <pc xml:id="A46511-001-a-0690">:</pc>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-0700">And</w>
     <w lemma="therefore" pos="av" xml:id="A46511-001-a-0710">therefore</w>
     <w lemma="do" pos="vvb" xml:id="A46511-001-a-0720">do</w>
     <w lemma="by" pos="acp" xml:id="A46511-001-a-0730">by</w>
     <w lemma="this" pos="d" xml:id="A46511-001-a-0740">this</w>
     <w lemma="our" pos="po" xml:id="A46511-001-a-0750">Our</w>
     <w lemma="royal" pos="j" xml:id="A46511-001-a-0760">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A46511-001-a-0770">Proclamation</w>
     <w lemma="publish" pos="vvb" xml:id="A46511-001-a-0780">Publish</w>
     <pc xml:id="A46511-001-a-0790">,</pc>
     <w lemma="notify" pos="vvi" reg="Notify" xml:id="A46511-001-a-0800">Notifie</w>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-0810">and</w>
     <w lemma="declare" pos="vvi" xml:id="A46511-001-a-0820">Declare</w>
     <pc xml:id="A46511-001-a-0830">,</pc>
     <w lemma="that" pos="cs" xml:id="A46511-001-a-0840">That</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-0850">the</w>
     <w lemma="parliament" pos="n1" xml:id="A46511-001-a-0860">Parliament</w>
     <w lemma="shall" pos="vmb" xml:id="A46511-001-a-0870">shall</w>
     <w lemma="be" pos="vvi" xml:id="A46511-001-a-0880">be</w>
     <w lemma="prorogue" pos="vvn" xml:id="A46511-001-a-0890">Prorogued</w>
     <w lemma="upon" pos="acp" xml:id="A46511-001-a-0900">upon</w>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-0910">and</w>
     <w lemma="from" pos="acp" xml:id="A46511-001-a-0920">from</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-0930">the</w>
     <w lemma="say" pos="vvd" xml:id="A46511-001-a-0940">said</w>
     <w lemma="ten" pos="ord" xml:id="A46511-001-a-0950">Tenth</w>
     <w lemma="day" pos="n1" xml:id="A46511-001-a-0960">day</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-0970">of</w>
     <w lemma="February" pos="nn1" rend="hi" xml:id="A46511-001-a-0980">February</w>
     <w lemma="until" pos="acp" xml:id="A46511-001-a-0990">until</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-1000">the</w>
     <w lemma="ten" pos="ord" xml:id="A46511-001-a-1010">Tenth</w>
     <w lemma="day" pos="n1" xml:id="A46511-001-a-1020">day</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-1030">of</w>
     <w lemma="May" pos="nn1" rend="hi" xml:id="A46511-001-a-1040">May</w>
     <w lemma="next" pos="ord" xml:id="A46511-001-a-1050">next</w>
     <pc xml:id="A46511-001-a-1060">:</pc>
     <w lemma="whereof" pos="crq" xml:id="A46511-001-a-1070">Whereof</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-1080">the</w>
     <w lemma="lord" pos="n2" xml:id="A46511-001-a-1090">Lords</w>
     <w lemma="spiritual" pos="j" xml:id="A46511-001-a-1100">Spiritual</w>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-1110">and</w>
     <w lemma="temporal" pos="j" xml:id="A46511-001-a-1120">Temporal</w>
     <pc xml:id="A46511-001-a-1130">,</pc>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-1140">and</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-1150">the</w>
     <w lemma="knight" pos="n2" xml:id="A46511-001-a-1160">Knights</w>
     <pc xml:id="A46511-001-a-1170">,</pc>
     <w lemma="citizen" pos="n2" xml:id="A46511-001-a-1180">Citizens</w>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-1190">and</w>
     <w lemma="burgess" pos="n2" xml:id="A46511-001-a-1200">Burgesses</w>
     <pc xml:id="A46511-001-a-1210">,</pc>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-1220">and</w>
     <w lemma="all" pos="d" xml:id="A46511-001-a-1230">all</w>
     <w lemma="other" pos="pi2-d" xml:id="A46511-001-a-1240">others</w>
     <w lemma="who" pos="crq" xml:id="A46511-001-a-1250">whom</w>
     <w lemma="it" pos="pn" xml:id="A46511-001-a-1260">it</w>
     <w lemma="may" pos="vmb" xml:id="A46511-001-a-1270">may</w>
     <w lemma="concern" pos="vvi" xml:id="A46511-001-a-1280">Concern</w>
     <pc xml:id="A46511-001-a-1290">,</pc>
     <w lemma="may" pos="vmb" xml:id="A46511-001-a-1300">may</w>
     <w lemma="hereby" pos="av" xml:id="A46511-001-a-1310">hereby</w>
     <w lemma="take" pos="vvi" xml:id="A46511-001-a-1320">take</w>
     <w lemma="notice" pos="n1" xml:id="A46511-001-a-1330">Notice</w>
     <pc xml:id="A46511-001-a-1340">,</pc>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-1350">and</w>
     <w lemma="order" pos="vvb" xml:id="A46511-001-a-1360">Order</w>
     <w lemma="their" pos="po" xml:id="A46511-001-a-1370">their</w>
     <w lemma="affair" pos="n2" xml:id="A46511-001-a-1380">Affairs</w>
     <w lemma="according" pos="av-j" xml:id="A46511-001-a-1390">accordingly</w>
     <pc xml:id="A46511-001-a-1400">:</pc>
     <w lemma="we" pos="pns" xml:id="A46511-001-a-1410">We</w>
     <w lemma="let" pos="vvg" xml:id="A46511-001-a-1420">letting</w>
     <w lemma="they" pos="pno" xml:id="A46511-001-a-1430">them</w>
     <w lemma="know" pos="vvi" xml:id="A46511-001-a-1440">know</w>
     <pc xml:id="A46511-001-a-1450">,</pc>
     <w lemma="that" pos="cs" xml:id="A46511-001-a-1460">that</w>
     <w lemma="we" pos="pns" xml:id="A46511-001-a-1470">We</w>
     <w lemma="will" pos="vmb" xml:id="A46511-001-a-1480">will</w>
     <w lemma="not" pos="xx" xml:id="A46511-001-a-1490">not</w>
     <w lemma="at" pos="acp" xml:id="A46511-001-a-1500">at</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-1510">the</w>
     <w lemma="say" pos="vvn" xml:id="A46511-001-a-1520">said</w>
     <w lemma="ten" pos="ord" xml:id="A46511-001-a-1530">Tenth</w>
     <w lemma="day" pos="n1" xml:id="A46511-001-a-1540">day</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-1550">of</w>
     <w lemma="February" pos="nn1" rend="hi" xml:id="A46511-001-a-1560">February</w>
     <w lemma="expect" pos="vvb" xml:id="A46511-001-a-1570">expect</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-1580">the</w>
     <w lemma="attendance" pos="n1" xml:id="A46511-001-a-1590">Attendance</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-1600">of</w>
     <w lemma="any" pos="d" xml:id="A46511-001-a-1610">any</w>
     <pc xml:id="A46511-001-a-1620">,</pc>
     <w lemma="but" pos="acp" xml:id="A46511-001-a-1630">but</w>
     <w lemma="only" pos="av-j" reg="only" xml:id="A46511-001-a-1640">onely</w>
     <w lemma="such" pos="d" xml:id="A46511-001-a-1650">such</w>
     <pc xml:id="A46511-001-a-1660">,</pc>
     <w lemma="as" pos="acp" xml:id="A46511-001-a-1670">as</w>
     <w lemma="be" pos="vvg" xml:id="A46511-001-a-1680">being</w>
     <w lemma="in" pos="acp" xml:id="A46511-001-a-1690">in</w>
     <w lemma="or" pos="cc" xml:id="A46511-001-a-1700">or</w>
     <w lemma="about" pos="acp" xml:id="A46511-001-a-1710">about</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-1720">the</w>
     <w lemma="city" pos="n2" xml:id="A46511-001-a-1730">Cities</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-1740">of</w>
     <w lemma="London" pos="nn1" rend="hi" xml:id="A46511-001-a-1750">London</w>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-1760">and</w>
     <hi xml:id="A46511-e200">
      <w lemma="Westminster" pos="nn1" xml:id="A46511-001-a-1770">Westminster</w>
      <pc xml:id="A46511-001-a-1780">,</pc>
     </hi>
     <w lemma="may" pos="vmb" xml:id="A46511-001-a-1790">may</w>
     <w lemma="attend" pos="vvi" xml:id="A46511-001-a-1800">Attend</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-1810">the</w>
     <w lemma="make" pos="vvg" xml:id="A46511-001-a-1820">making</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-1830">the</w>
     <w lemma="say" pos="j-vn" xml:id="A46511-001-a-1840">said</w>
     <w lemma="prorogation" pos="n1" xml:id="A46511-001-a-1850">Prorogation</w>
     <pc xml:id="A46511-001-a-1860">,</pc>
     <w lemma="as" pos="acp" xml:id="A46511-001-a-1870">as</w>
     <w lemma="heretofore" pos="av" xml:id="A46511-001-a-1880">heretofore</w>
     <w lemma="in" pos="acp" xml:id="A46511-001-a-1890">in</w>
     <w lemma="like" pos="j" xml:id="A46511-001-a-1900">like</w>
     <w lemma="case" pos="n2" xml:id="A46511-001-a-1910">Cases</w>
     <w lemma="have" pos="vvz" xml:id="A46511-001-a-1920">hath</w>
     <w lemma="be" pos="vvn" xml:id="A46511-001-a-1930">been</w>
     <w lemma="accustom" pos="vvn" xml:id="A46511-001-a-1940">accustomed</w>
     <pc unit="sentence" xml:id="A46511-001-a-1950">.</pc>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-1960">And</w>
     <w lemma="we" pos="pns" xml:id="A46511-001-a-1970">We</w>
     <w lemma="do" pos="vvb" xml:id="A46511-001-a-1980">do</w>
     <w lemma="also" pos="av" xml:id="A46511-001-a-1990">also</w>
     <w lemma="hereby" pos="av" xml:id="A46511-001-a-2000">hereby</w>
     <w lemma="further" pos="av-j" xml:id="A46511-001-a-2010">further</w>
     <w lemma="declare" pos="vvb" xml:id="A46511-001-a-2020">Declare</w>
     <w lemma="our" pos="po" xml:id="A46511-001-a-2030">Our</w>
     <w lemma="royal" pos="j" xml:id="A46511-001-a-2040">Royal</w>
     <w lemma="pleasure" pos="n1" xml:id="A46511-001-a-2050">Pleasure</w>
     <pc xml:id="A46511-001-a-2060">,</pc>
     <w lemma="that" pos="cs" xml:id="A46511-001-a-2070">That</w>
     <w lemma="we" pos="pns" xml:id="A46511-001-a-2080">We</w>
     <w lemma="shall" pos="vmb" xml:id="A46511-001-a-2090">shall</w>
     <w lemma="not" pos="xx" xml:id="A46511-001-a-2100">not</w>
     <w lemma="expect" pos="vvi" xml:id="A46511-001-a-2110">expect</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-2120">the</w>
     <w lemma="attendance" pos="n1" xml:id="A46511-001-a-2130">Attendance</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-2140">of</w>
     <w lemma="our" pos="po" xml:id="A46511-001-a-2150">Our</w>
     <w lemma="house" pos="n2" xml:id="A46511-001-a-2160">Houses</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-2170">of</w>
     <w lemma="parliament" pos="n1" xml:id="A46511-001-a-2180">Parliament</w>
     <w lemma="upon" pos="acp" xml:id="A46511-001-a-2190">upon</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-2200">the</w>
     <w lemma="say" pos="vvd" xml:id="A46511-001-a-2210">said</w>
     <w lemma="ten" pos="ord" xml:id="A46511-001-a-2220">Tenth</w>
     <w lemma="day" pos="n1" xml:id="A46511-001-a-2230">day</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-2240">of</w>
     <hi xml:id="A46511-e210">
      <w lemma="May" pos="nn1" xml:id="A46511-001-a-2250">May</w>
      <pc xml:id="A46511-001-a-2260">,</pc>
     </hi>
     <w lemma="but" pos="acp" xml:id="A46511-001-a-2270">but</w>
     <w lemma="intend" pos="vvb" reg="intent" xml:id="A46511-001-a-2280">intend</w>
     <w lemma="at" pos="acp" xml:id="A46511-001-a-2290">at</w>
     <w lemma="that" pos="d" xml:id="A46511-001-a-2300">that</w>
     <w lemma="time" pos="n1" xml:id="A46511-001-a-2310">time</w>
     <w lemma="a" pos="d" xml:id="A46511-001-a-2320">a</w>
     <w lemma="further" pos="jc" xml:id="A46511-001-a-2330">further</w>
     <w lemma="prorogation" pos="n1" xml:id="A46511-001-a-2340">Prorogation</w>
     <w lemma="to" pos="acp" xml:id="A46511-001-a-2350">to</w>
     <w lemma="a" pos="d" xml:id="A46511-001-a-2360">a</w>
     <w lemma="more" pos="avc-d" xml:id="A46511-001-a-2370">more</w>
     <w lemma="proper" pos="j" xml:id="A46511-001-a-2380">proper</w>
     <w lemma="season" pos="n1" xml:id="A46511-001-a-2390">season</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-2400">of</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-2410">the</w>
     <w lemma="year" pos="n1" xml:id="A46511-001-a-2420">year</w>
     <pc xml:id="A46511-001-a-2430">,</pc>
     <w lemma="unless" pos="cs" xml:id="A46511-001-a-2440">unless</w>
     <w lemma="some" pos="d" xml:id="A46511-001-a-2450">some</w>
     <w lemma="extraordinary" pos="j" xml:id="A46511-001-a-2460">Extraordinary</w>
     <w lemma="occasion" pos="n1" xml:id="A46511-001-a-2470">Occasion</w>
     <w lemma="require" pos="vvz" xml:id="A46511-001-a-2480">requires</w>
     <w lemma="their" pos="po" xml:id="A46511-001-a-2490">their</w>
     <w lemma="sit" pos="vvg" xml:id="A46511-001-a-2500">Sitting</w>
     <pc xml:id="A46511-001-a-2510">,</pc>
     <w lemma="whereof" pos="crq" xml:id="A46511-001-a-2520">whereof</w>
     <w lemma="we" pos="pns" xml:id="A46511-001-a-2530">We</w>
     <w lemma="will" pos="vmb" xml:id="A46511-001-a-2540">will</w>
     <w lemma="give" pos="vvi" xml:id="A46511-001-a-2550">give</w>
     <w lemma="convenient" pos="j" xml:id="A46511-001-a-2560">Convenient</w>
     <w lemma="notice" pos="n1" xml:id="A46511-001-a-2570">Notice</w>
     <w lemma="by" pos="acp" xml:id="A46511-001-a-2580">by</w>
     <w lemma="our" pos="po" xml:id="A46511-001-a-2590">Our</w>
     <w lemma="royal" pos="j" xml:id="A46511-001-a-2600">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A46511-001-a-2610">Proclamation</w>
     <pc unit="sentence" xml:id="A46511-001-a-2620">.</pc>
    </p>
    <closer xml:id="A46511-e220">
     <dateline xml:id="A46511-e230">
      <w lemma="give" pos="vvn" xml:id="A46511-001-a-2630">Given</w>
      <w lemma="at" pos="acp" xml:id="A46511-001-a-2640">at</w>
      <w lemma="our" pos="po" xml:id="A46511-001-a-2650">Our</w>
      <w lemma="court" pos="n1" xml:id="A46511-001-a-2660">Court</w>
      <w lemma="at" pos="acp" xml:id="A46511-001-a-2670">at</w>
      <w lemma="Whitehall" pos="nn1" rend="hi" xml:id="A46511-001-a-2680">Whitehall</w>
      <date xml:id="A46511-e250">
       <w lemma="the" pos="d" xml:id="A46511-001-a-2690">the</w>
       <w lemma="eight" pos="ord" xml:id="A46511-001-a-2700">Eighth</w>
       <w lemma="day" pos="n1" xml:id="A46511-001-a-2710">Day</w>
       <w lemma="of" pos="acp" xml:id="A46511-001-a-2720">of</w>
       <w lemma="January" pos="nn1" rend="hi" xml:id="A46511-001-a-2730">January</w>
       <w lemma="1685/6." pos="crd" xml:id="A46511-001-a-2750">1685/6.</w>
       <pc unit="sentence" xml:id="A46511-001-a-2770"/>
       <w lemma="in" pos="acp" xml:id="A46511-001-a-2780">In</w>
       <w lemma="the" pos="d" xml:id="A46511-001-a-2790">the</w>
       <w lemma="first" pos="ord" xml:id="A46511-001-a-2800">First</w>
       <w lemma="year" pos="n1" xml:id="A46511-001-a-2810">Year</w>
       <w lemma="of" pos="acp" xml:id="A46511-001-a-2820">of</w>
       <w lemma="our" pos="po" xml:id="A46511-001-a-2830">Our</w>
       <w lemma="reign" pos="n1" xml:id="A46511-001-a-2840">Reign</w>
       <pc unit="sentence" xml:id="A46511-001-a-2850">.</pc>
      </date>
     </dateline>
     <lb xml:id="A46511-e270"/>
     <w lemma="God" pos="nn1" xml:id="A46511-001-a-2860">God</w>
     <w lemma="save" pos="vvb" xml:id="A46511-001-a-2870">save</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-2880">the</w>
     <w lemma="King" pos="n1" xml:id="A46511-001-a-2890">King</w>
     <pc unit="sentence" xml:id="A46511-001-a-2891">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A46511-e280">
   <div type="colophon" xml:id="A46511-e290">
    <p xml:id="A46511-e300">
     <hi xml:id="A46511-e310">
      <w lemma="LONDON" pos="nn1" xml:id="A46511-001-a-2910">LONDON</w>
      <pc xml:id="A46511-001-a-2920">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A46511-001-a-2930">Printed</w>
     <w lemma="by" pos="acp" xml:id="A46511-001-a-2940">by</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-2950">the</w>
     <w lemma="assign" pos="vvz" xml:id="A46511-001-a-2960">Assigns</w>
     <w lemma="of" pos="acp" xml:id="A46511-001-a-2970">of</w>
     <hi xml:id="A46511-e320">
      <w lemma="John" pos="nn1" xml:id="A46511-001-a-2980">John</w>
      <w lemma="bill" pos="n1" xml:id="A46511-001-a-2990">Bill</w>
     </hi>
     <w lemma="decease" pos="j-vn" reg="Deceased" xml:id="A46511-001-a-3000">Deceas'd</w>
     <pc xml:id="A46511-001-a-3010">:</pc>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-3020">And</w>
     <w lemma="by" pos="acp" xml:id="A46511-001-a-3030">by</w>
     <hi xml:id="A46511-e330">
      <w lemma="Henry" pos="nn1" xml:id="A46511-001-a-3040">Henry</w>
      <w lemma="hill" pos="n2" xml:id="A46511-001-a-3050">Hills</w>
      <pc xml:id="A46511-001-a-3060">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A46511-001-a-3070">and</w>
     <hi xml:id="A46511-e340">
      <w lemma="Thomas" pos="nn1" xml:id="A46511-001-a-3080">Thomas</w>
      <w lemma="newcomb" pos="nn1" xml:id="A46511-001-a-3090">Newcomb</w>
      <pc xml:id="A46511-001-a-3100">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A46511-001-a-3110">Printers</w>
     <w lemma="to" pos="acp" xml:id="A46511-001-a-3120">to</w>
     <w lemma="the" pos="d" xml:id="A46511-001-a-3130">the</w>
     <w lemma="king" pos="n2" xml:id="A46511-001-a-3140">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="A46511-001-a-3150">most</w>
     <w lemma="excellent" pos="j" xml:id="A46511-001-a-3160">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A46511-001-a-3170">Majesty</w>
     <pc unit="sentence" xml:id="A46511-001-a-3180">.</pc>
     <w lemma="1685/6." pos="crd" xml:id="A46511-001-a-3200">1685/6.</w>
     <pc unit="sentence" xml:id="A46511-001-a-3220"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
